import React from 'react';
import { useState } from 'react';
import Day from './Day'
import './App.css'


function Month(props) {

    const [selection , setSelection] = useState(1);
    const months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

    let monthsArray = months.map((month, key) => {
        return (<option className='App' id='opt' key={key} value={month}> {month} </option>);
    });



    function onChangeHandler(event) {
        setSelection(event.target.value);

    }
    return (
        <div>
                    <select onChange={onChangeHandler}>{monthsArray}</select>
                   
                    <Day month={selection}></Day>
        </div>);

}
    

export default Month;



